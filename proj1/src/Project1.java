import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import proj1.ParseError;
import proj1.Parser;
import proj1.Tokenizer;

class Project1 {
    static void printUsage() {
        System.err.println("Usage: p1 [-ast] [-debug] [-noout] FILE");
    }

    public static void main(String[] args) {
        HashMap<String, String> options = parseArgs(args);

        try {
            FileReader in = new FileReader(options.get("filename"));
            Tokenizer tk = new Tokenizer(in, options.get("-debug").equals("true"));
            Parser parser = new Parser(tk, options.get("-debug").equals("true"));

            if (options.get("-ast").equals("true")) {
                parser.printAST();
            }

            if (!options.get("-noout").equals("true")) {
                // This switch does nothing for Project 1.
                //System.out.println();
            }
        }
        catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
            System.exit(2);
        }
        catch (ParseError e) {
            System.err.println("ParseError: " + e.getMessage());
            System.exit(3);
        }
    }

    static HashMap<String, String> parseArgs(String[] args) {
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("-ast", "false");
        options.put("-debug", "false");
        options.put("-noout", "false");

        for (String arg : args) {
            if (arg.startsWith("-")) {
                if (options.containsKey(arg)) {
                    options.put(arg, "true");
                }
                else {
                    // Invalid switch. Ignore it, like the reference parser
                    // does.
                }
            }
            // The reference parser always treats the last argument as the
            // filename, even if it starts with a hyphen. This simulates that
            // behavior.
            options.put("filename", arg);
        }

        if (options.get("filename") == null) {
            printUsage();
            System.exit(1);
        }
        return options;
    }
}
