package proj1;

public class ParseError extends Exception {
    ParseError(String message) {
        super(message);
    }
}
