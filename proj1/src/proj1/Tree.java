package proj1;

class Tree {
    String name;
    Tree firstChild;
    Tree nextSibling;

    public Tree(String name) {
        this.name = name;
    }

    public void print(String prefix) {
        System.out.println(prefix + this.name);
        if (this.firstChild != null) {
            this.firstChild.print(prefix + ".");
        }
        if (this.nextSibling != null) {
            this.nextSibling.print(prefix);
        }
    }
}
