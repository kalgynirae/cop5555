package proj1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.Character;
import java.lang.StringBuilder;
import java.util.HashSet;
import java.util.Set;

import proj1.ParseError;
import proj1.Token;

public class Tokenizer {
    static Set<Character> Letter;
    static Set<Character> Digit;
    static Set<Character> Operator_symbol;

    boolean printTokens;
    BufferedReader in;
    Token nextToken;

    public Tokenizer(Reader in, boolean printTokens)
            throws IOException, ParseError {
        this.in = new BufferedReader(in);
        this.printTokens = printTokens;
        this.readNextToken();
    }

    public Token peek() {
        return this.nextToken;
    }

    public void readNextToken() throws ParseError {
        Token t = null;
        try {
            while (t == null) {
                // Get the first character
                this.in.mark(1);
                int i = this.in.read();
                this.in.reset();

                if (i == -1) {
                    t = new Token("eof");
                }
                else {
                    char c = (char) i;
                    if (Letter.contains(c)) {
                        t = this.readIdentifier();
                    }
                    else if (Digit.contains(c)) {
                        t = this.readInteger();
                    }
                    else if (Operator_symbol.contains(c)) {
                        if (c == '/') {
                            // See if the second character is another '/'
                            this.in.mark(2);
                            c = (char) this.in.read();
                            c = (char) this.in.read();
                            this.in.reset();
                            if (c == '/') {
                                this.readComment();
                            }
                            else {
                                t = this.readOperator();
                            }
                        }
                        else {
                            t = this.readOperator();
                        }
                    }
                    else if (c == '\'') {
                        t = this.readString();
                    }
                    else if (c == ' ' || c == '\t' || c == '\n') {
                        this.readSpaces();
                    }
                    else if (c == '(' || c == ')' || c == ';' || c == ',') {
                        t = this.readPunction();
                    }
                }
            }
        }
        catch (IOException e) {
            t = new Token("IOException");
        }
        this.nextToken = t;
        if (this.printTokens) {
            System.out.print("[token] ");
            System.out.println(t);
        }
    }

    private Token readIdentifier() throws IOException, ParseError {
        StringBuilder sb = new StringBuilder();
        while (true) {
            this.in.mark(1);
            char c = (char) this.in.read();
            if (Letter.contains(c) || Digit.contains(c) || c == '_') {
                sb.append(c);
            }
            else {
                this.in.reset();
                break;
            }
        }
        String value = sb.toString();
        String keywords = ("/let/in/fn/where/aug/or/not/gr/ls/le/eq/ne" +
                           "/true/false/nil/dummy/within/and/rec/");
        if (keywords.contains("/" + value + "/")) {
            return new Token(value);
        }
        else {
            return new Token("ID", sb.toString());
        }
    }

    private Token readInteger() throws IOException, ParseError {
        StringBuilder sb = new StringBuilder();
        while (true) {
            this.in.mark(1);
            char c = (char) this.in.read();
            if (Digit.contains(c)) {
                sb.append(c);
            }
            else {
                this.in.reset();
                break;
            }
        }
        return new Token("INT", sb.toString());
    }

    private Token readOperator() throws IOException, ParseError {
        StringBuilder sb = new StringBuilder();
        while (true) {
            this.in.mark(1);
            char c = (char) this.in.read();
            if (Operator_symbol.contains(c)) {
                sb.append(c);
            }
            else {
                this.in.reset();
                break;
            }
        }
        return new Token(sb.toString());
    }

    private Token readString() throws IOException, ParseError {
        char c = (char) this.in.read(); assert c == '\'';
        StringBuilder sb = new StringBuilder();
        while (true) {
            c = (char) this.in.read();
            if (Letter.contains(c) || Digit.contains(c) ||
                    Operator_symbol.contains(c) || c == '(' || c == ')' ||
                    c == ';' || c == ',' || c == ' ') {
                sb.append(c);
            }
            else if (c == '\\') {
                c = (char) this.in.read();
                if (c == 't') {
                    sb.append('\t');
                }
                else if (c == 'n') {
                    sb.append('\n');
                }
                else if (c == '\\' || c == '\'') {
                    sb.append(c);
                }
                else {
                    throw new ParseError("Invalid character '" + c + "'");
                }
            }
            else if (c == '\'') {
                break;
            }
            else {
                throw new ParseError("Invalid character '" + c + "'");
            }
        }
        return new Token("STR", sb.toString());
    }

    private void readSpaces() throws IOException, ParseError {
        while (true) {
            this.in.mark(1);
            char c = (char) this.in.read();
            if (c == ' ' || c == '\t' || c == '\n') {
            }
            else {
                this.in.reset();
                return;
            }
        }
    }

    private void readComment() throws IOException, ParseError {
        char c = (char) this.in.read(); assert c == '/';
        c = (char) this.in.read(); assert c == '/';
        while (true) {
            this.in.mark(1);
            c = (char) this.in.read();
            if (c == '\'' || c == '(' || c == ')' || c == ';' || c == ',' ||
                    c == '\\' || c == ' ' || c == '\t' || Letter.contains(c) ||
                    Digit.contains(c) || Operator_symbol.contains(c)) {
            }
            else if (c == '\n') {
                break;
            }
            else {
                throw new ParseError("Invalid character '" + c + "'");
            }
        }
    }

    private Token readPunction() throws IOException, ParseError {
        char c = (char) this.in.read();
        assert c == '(' || c == ')' || c == ';' || c == ',';
        return new Token(String.valueOf(c));
    }

    static {
        Letter = new HashSet<Character>();
        Letter.add('A');
        Letter.add('B');
        Letter.add('C');
        Letter.add('D');
        Letter.add('E');
        Letter.add('F');
        Letter.add('G');
        Letter.add('H');
        Letter.add('I');
        Letter.add('J');
        Letter.add('K');
        Letter.add('L');
        Letter.add('M');
        Letter.add('N');
        Letter.add('O');
        Letter.add('P');
        Letter.add('Q');
        Letter.add('R');
        Letter.add('S');
        Letter.add('T');
        Letter.add('U');
        Letter.add('V');
        Letter.add('W');
        Letter.add('X');
        Letter.add('Y');
        Letter.add('Z');
        Letter.add('a');
        Letter.add('b');
        Letter.add('c');
        Letter.add('d');
        Letter.add('e');
        Letter.add('f');
        Letter.add('g');
        Letter.add('h');
        Letter.add('i');
        Letter.add('j');
        Letter.add('k');
        Letter.add('l');
        Letter.add('m');
        Letter.add('n');
        Letter.add('o');
        Letter.add('p');
        Letter.add('q');
        Letter.add('r');
        Letter.add('s');
        Letter.add('t');
        Letter.add('u');
        Letter.add('v');
        Letter.add('w');
        Letter.add('x');
        Letter.add('y');
        Letter.add('z');

        Digit = new HashSet<Character>();
        Digit.add('0');
        Digit.add('1');
        Digit.add('2');
        Digit.add('3');
        Digit.add('4');
        Digit.add('5');
        Digit.add('6');
        Digit.add('7');
        Digit.add('8');
        Digit.add('9');

        Operator_symbol = new HashSet<Character>();
        Operator_symbol.add('+');
        Operator_symbol.add('-');
        Operator_symbol.add('*');
        Operator_symbol.add('<');
        Operator_symbol.add('>');
        Operator_symbol.add('&');
        Operator_symbol.add('.');
        Operator_symbol.add('@');
        Operator_symbol.add('/');
        Operator_symbol.add(':');
        Operator_symbol.add('=');
        Operator_symbol.add('~');
        Operator_symbol.add('|');
        Operator_symbol.add('$');
        Operator_symbol.add('!');
        Operator_symbol.add('#');
        Operator_symbol.add('%');
        Operator_symbol.add('^');
        Operator_symbol.add('_');
        Operator_symbol.add('[');
        Operator_symbol.add(']');
        Operator_symbol.add('{');
        Operator_symbol.add('}');
        Operator_symbol.add('"');
        Operator_symbol.add('`');
        Operator_symbol.add('?');
    }
}
