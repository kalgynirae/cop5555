package proj1;

import java.io.IOException;
import java.util.EmptyStackException;
import java.util.Stack;

import proj1.ParseError;
import proj1.Tree;

public class Parser {
    Tree ast;
    boolean debug;
    Tokenizer tokenizer;
    Stack<Tree> trees;

    public Parser(Tokenizer tokenizer, boolean debug) throws ParseError {
        this.debug = debug;
        this.tokenizer = tokenizer;
        this.trees = new Stack<Tree>();
        E();
        consume("eof");
        try {
            this.ast = this.trees.pop();
        }
        catch (EmptyStackException e) {
            throw new ParseError("Ended with no trees on the stack");
        }
        if (!this.trees.empty()) {
            throw new ParseError("Ended with multiple trees on the stack");
        }
    }

    public void printAST() {
        this.ast.print("");
    }

    void buildTree(String name, int subtreeCount) {
        if (this.debug) {
            System.out.println("buildTree(" + name + ", " + subtreeCount + ")");
        }
        Tree newTree = new Tree(name);
        if (subtreeCount > 0) {
            Tree t = this.trees.pop();
            for (int i = 1; i < subtreeCount; i++) {
                Tree q = this.trees.pop();
                q.nextSibling = t;
                t = q;
            }
            newTree.firstChild = t;
        }
        this.trees.push(newTree);
    }

    void consume(String type) throws ParseError {
        Token t = new Token(type);
        if (this.tokenizer.peek().equals(t)) {
            this.tokenizer.readNextToken();
        }
        else {
            throw new ParseError("[consume] Expected " + t + "; got " + this.tokenizer.peek());
        }
    }

    boolean tryConsume(String type) throws ParseError {
        Token t = new Token(type);
        if (this.tokenizer.peek().equals(t)) {
            this.tokenizer.readNextToken();
            return true;
        }
        else {
            return false;
        }
    }

    void read(String type) throws ParseError {
        Token t = new Token(type);
        Token read = this.tokenizer.peek();
        if (read.equals(t)) {
            buildTree(read.toString(), 0);
            this.tokenizer.readNextToken();
        }
        else {
            throw new ParseError("[read] Expected " + t + "; got " + read);
        }
    }

    boolean tryRead(String type) throws ParseError {
        Token t = new Token(type);
        Token read = this.tokenizer.peek();
        if (read.equals(t)) {
            buildTree(read.toString(), 0);
            this.tokenizer.readNextToken();
            return true;
        }
        else {
            return false;
        }
    }

    boolean test(String type) throws ParseError {
        Token t = new Token(type);
        return this.tokenizer.peek().equals(t);
    }

    void E() throws ParseError {
        if (tryConsume("let")) {
            D();
            consume("in");
            E();
            buildTree("let", 2);
        }
        else if (tryConsume("fn")) {
            Vb();
            int n = 1;
            while (!tryConsume(".")) {
                Vb();
                n++;
            }
            E();
            buildTree("lambda", n + 1);
        }
        else {
            Ew();
        }
    }

    void Ew() throws ParseError {
        T();
        if (tryConsume("where")) {
            Dr();
            buildTree("where", 2);
        }
    }

    void T() throws ParseError {
        Ta();
        int n = 1;
        while (tryConsume(",")) {
            Ta();
            n++;
        }
        if (n > 1) {
            buildTree("tau", n);
        }
    }

    void Ta() throws ParseError {
        Tc();
        while (tryConsume("aug")) {
            Tc();
            buildTree("aug", 2);
        }
    }

    void Tc() throws ParseError {
        B();
        if (tryConsume("->")) {
            Tc();
            consume("|");
            Tc();
            buildTree("->", 3);
        }
    }

    void B() throws ParseError {
        Bt();
        while (tryConsume("or")) {
            Bt();
            buildTree("or", 2);
        }
    }

    void Bt() throws ParseError {
        Bs();
        while (tryConsume("&")) {
            Bs();
            buildTree("&", 2);
        }
    }

    void Bs() throws ParseError {
        if (tryConsume("not")) {
            Bp();
            buildTree("not", 1);
        }
        else {
            Bp();
        }
    }

    void Bp() throws ParseError {
        A();
        if (tryConsume("gr") || tryConsume(">")) {
            A();
            buildTree("gr", 2);
        }
        else if (tryConsume("ge") || tryConsume(">=")) {
            A();
            buildTree("ge", 2);
        }
        else if (tryConsume("ls") || tryConsume("<")) {
            A();
            buildTree("ls", 2);
        }
        else if (tryConsume("le") || tryConsume("<=")) {
            A();
            buildTree("le", 2);
        }
        else if (tryConsume("eq")) {
            A();
            buildTree("eq", 2);
        }
        else if (tryConsume("ne")) {
            A();
            buildTree("ne", 2);
        }
    }

    void A() throws ParseError {
        if (tryConsume("+")) {
            At();
        }
        else if (tryConsume("-")) {
            At();
            buildTree("neg", 1);
        }
        else {
            At();
        }
        while (true) {
            if (tryConsume("+")) {
                At();
                buildTree("+", 2);
            }
            else if (tryConsume("-")) {
                At();
                buildTree("-", 2);
            }
            else {
                break;
            }
        }
    }

    void At() throws ParseError {
        Af();
        while (true) {
            if (tryConsume("*")) {
                Af();
                buildTree("*", 2);
            }
            else if (tryConsume("/")) {
                Af();
                buildTree("/", 2);
            }
            else {
                break;
            }
        }
    }

    void Af() throws ParseError {
        Ap();
        if (tryConsume("**")) {
            Af();
            buildTree("**", 2);
        }
    }

    void Ap() throws ParseError {
        R();
        while (tryConsume("@")) {
            read("ID");
            R();
            buildTree("@", 3);
        }
    }

    void R() throws ParseError {
        Rn();
        while (true) {
            try {
                Rn();
            }
            catch (ParseError e) {
                break;
            }
            buildTree("gamma", 2);
        }
    }

    void Rn() throws ParseError {
        if (tryRead("ID")) {
        }
        else if (tryRead("INT")) {
        }
        else if (tryRead("STR")) {
        }
        else if (tryConsume("true")) {
            buildTree("<true>", 0);
        }
        else if (tryConsume("false")) {
            buildTree("<false>", 0);
        }
        else if (tryConsume("nil")) {
            buildTree("<nil>", 0);
        }
        else if (tryConsume("(")) {
            E();
            consume(")");
        }
        else if (tryConsume("dummy")) {
            buildTree("<dummy>", 0);
        }
        else {
            throw new ParseError("[Rn] Expected ID, INT, STR, true, false, nil, (, or dummy");
        }
    }

    void D() throws ParseError {
        Da();
        if (tryConsume("within")) {
            D();
            buildTree("within", 2);
        }
    }

    void Da() throws ParseError {
        Dr();
        int n = 1;
        while (tryConsume("and")) {
            Dr();
            n++;
        }
        if (n > 1) {
            buildTree("and", n);
        }
    }

    void Dr() throws ParseError {
        if (tryConsume("rec")) {
            Db();
            buildTree("rec", 1);
        }
        else {
            Db();
        }
    }

    void Db() throws ParseError {
        if (test("ID")) {
            boolean multiple = Vl();
            if (multiple || test("=")) {
                consume("=");
                E();
                buildTree("=", 2);
            }
            else {
                Vb();
                int n = 1;
                while (!tryConsume("=")) {
                    Vb();
                    n++;
                }
                E();
                buildTree("function_form", n + 2);
            }
        }
        else if (tryConsume("(")) {
            D();
            consume(")");
        }
        else {
            throw new ParseError("[Db] Expected ID or (");
        }
    }

    void Vb() throws ParseError {
        if (tryRead("ID")) {
        }
        else if (tryConsume("(")) {
            if (tryConsume(")")) {
                buildTree("()", 0);
            }
            else {
                Vl();
                consume(")");
            }
        }
        else {
            throw new ParseError("[Vb] Expected ID or (");
        }
    }

    boolean Vl() throws ParseError {
        read("ID");
        int n = 1;
        while (tryConsume(",")) {
            read("ID");
            n++;
        }
        if (n > 1) {
            buildTree(",", n);
            return true;
        }
        else {
            return false;
        }
    }
}
