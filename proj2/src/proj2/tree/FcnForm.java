package proj2.tree;

import java.util.ArrayList;

import proj2.tree.Tree;

public class FcnForm extends Tree {
    Tree _standardize(boolean debug) {
        if (debug) System.err.println("Standardizing " + this);

        Tree p, e;
        ArrayList<Tree> vs = this.children();
        p = vs.remove(0);
        e = vs.remove(vs.size() - 1);
        assert vs.size() > 0;

        Tree eq = new Eq();
        eq.firstChild = p;
        Tree last = p;
        for (Tree v : vs) {
            Tree lambda = new Lambda();
            last.nextSibling = lambda;
            lambda.firstChild = v;
            last = v;
        }
        last.nextSibling = e;

        eq.nextSibling = this.nextSibling;
        return eq;
    }

    public String toString() {
        return "function_form";
    }
}
