package proj2.tree;

import proj2.tree.Tree;

public class Let extends Tree {
    Tree _standardize(boolean debug) {
        if (debug) System.err.println("Standardizing " + this);

        Tree x = this.firstChild.firstChild;
        Tree p = this.firstChild.nextSibling;
        Tree e = x.nextSibling;

        Tree gamma = new Gamma();
        Tree lambda = new Lambda();
        gamma.firstChild = lambda;
        lambda.firstChild = x;
        x.nextSibling = p;
        lambda.nextSibling = e;

        gamma.nextSibling = this.nextSibling;
        return gamma;
    }
}
