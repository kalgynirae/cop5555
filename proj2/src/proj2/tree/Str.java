package proj2.tree;

import proj2.tree.TreeValue;

public class Str extends TreeValue {
    public Str(String value) {
        super(value);
    }

    public String toString() {
        String fixed = this.value.replace("\\", "\\\\")
                                 .replace("'", "\\'")
                                 .replace("\n", "\\n")
                                 .replace("\t", "\\t");
        return "<STR:'" + fixed + "'>";
    }
}
