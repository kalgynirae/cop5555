package proj2.tree;

import proj2.tree.Tree;

public class Where extends Tree {
    Tree _standardize(boolean debug) {
        if (debug) System.err.println("Standardizing " + this);

        Tree p = this.firstChild;
        Tree x = p.nextSibling.firstChild;
        Tree e = x.nextSibling;
        assert x.firstChild == null;

        p.nextSibling = null;

        Tree tree = new Gamma();
        tree.firstChild = new Lambda();
        tree.firstChild.nextSibling = e;
        tree.firstChild.firstChild = x;
        x.nextSibling = p;

        tree.nextSibling = this.nextSibling;
        return tree;
    }
}
