package proj2.tree;

import proj2.tree.Tree;

public class At extends Tree {
    Tree _standardize(boolean debug) {
        if (debug) System.err.println("Standardizing " + this);

        Tree e1, n, e2;
        e1 = this.firstChild;
        n = e1.nextSibling;
        e2 = n.nextSibling;
        e1.nextSibling = null;

        Tree g1, g2;
        g1 = new Gamma();
        g2 = new Gamma();
        g1.firstChild = g2;
        g2.nextSibling = e2;
        g2.firstChild = n;
        n.nextSibling = e1;

        g1.nextSibling = this.nextSibling;
        return g1;
    }

    public String toString() {
        return "@";
    }
}
