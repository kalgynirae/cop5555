package proj2.tree;

import java.util.ArrayList;

import proj2.tree.Tree;

public class And extends Tree {
    Tree _standardize(boolean debug) {
        if (debug) System.err.println("Standardizing " + this);

        ArrayList<Tree> xs = new ArrayList<Tree>();
        ArrayList<Tree> es = new ArrayList<Tree>();
        for (Tree eq : this.children()) {
            xs.add(eq.firstChild);
            es.add(eq.firstChild.nextSibling);
            eq.firstChild.nextSibling = null;
        }

        Tree eq, comma, tau, comma_last_child, tau_last_child;
        eq = new Eq();
        comma = new Comma();
        tau = new Tau();
        eq.firstChild = comma;
        comma.nextSibling = tau;

        comma.firstChild = xs.remove(0);
        tau.firstChild = es.remove(0);
        comma_last_child = comma.firstChild;
        tau_last_child = tau.firstChild;
        while (xs.size() > 0) {
            comma_last_child.nextSibling = xs.remove(0);
            tau_last_child.nextSibling = es.remove(0);
            comma_last_child = comma_last_child.nextSibling;
            tau_last_child = tau_last_child.nextSibling;
        }

        eq.nextSibling = this.nextSibling;
        return eq;
    }
}
