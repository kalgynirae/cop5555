package proj2.tree;

import proj2.tree.Tree;

public abstract class TreeValue extends Tree {
    public String value;

    public TreeValue(String value) {
        this.value = value;
    }

    public void print(String prefix) {
        System.out.println(prefix + this);
        if (this.firstChild != null) {
            this.firstChild.print(prefix + ".");
        }
        if (this.nextSibling != null) {
            this.nextSibling.print(prefix);
        }
    }

    public String toString() {
        return this.value;
    }
}
