package proj2.tree;

import proj2.tree.Tree;

public class Rec extends Tree {
    Tree _standardize(boolean debug) {
        if (debug) System.err.println("Standardizing " + this);

        Tree e, x;
        x = this.firstChild.firstChild;
        e = x.nextSibling;

        Tree eq, gamma, ystar, lambda;
        eq = new Eq();
        gamma = new Gamma();
        ystar = new Value("<Y*>");
        lambda = new Lambda();

        eq.firstChild = x;
        x.nextSibling = gamma;
        gamma.firstChild = ystar;
        ystar.nextSibling = lambda;
        lambda.firstChild = new Id((Id) x);
        lambda.firstChild.nextSibling = e;

        eq.nextSibling = this.nextSibling;
        return eq;
    }
}
