package proj2.tree;

import proj2.tree.TreeValue;

public class Id extends TreeValue {
    public Id(String value) {
        super(value.substring(4, value.length() - 1));
    }

    public Id(Id id) {
        super(id.value);
    }

    public String toString() {
        return "<ID:" + this.value + ">";
    }
}
