package proj2.tree;

import java.util.ArrayList;

public abstract class Tree {
    public Tree firstChild;
    public Tree nextSibling;

    public ArrayList<Tree> children() {
        ArrayList<Tree> al = new ArrayList<Tree>();
        Tree current = this.firstChild;
        while (current != null) {
            al.add(current);
            current = current.nextSibling;
        }
        return al;
    }

    public String toString() {
        return this.getClass().getSimpleName().toLowerCase();
    }

    public void print(String prefix) {
        System.out.println(prefix + this);
        if (this.firstChild != null) {
            this.firstChild.print(prefix + ".");
        }
        if (this.nextSibling != null && !prefix.equals("")) {
            this.nextSibling.print(prefix);
        }
    }

    public Tree standardize(boolean debug) {
        // Standardize children
        if (this.firstChild != null) {
            this.firstChild = this.firstChild.standardize(debug);
        }
        // Standardize next sibling
        if (this.nextSibling != null) {
            this.nextSibling = this.nextSibling.standardize(debug);
        }
        // Standardize self
        if (debug) {
            System.out.println("About to standardize " + this + ", which looks like:");
            this.print("");
        }
        return this._standardize(debug);
    }

    Tree _standardize(boolean debug) {
        if (debug) System.err.println("Skipping " + this);
        return this;
    }
}
