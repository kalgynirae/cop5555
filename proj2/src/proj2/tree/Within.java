package proj2.tree;

import proj2.tree.Tree;

public class Within extends Tree {
    Tree _standardize(boolean debug) {
        if (debug) System.err.println("Standardizing " + this);

        Tree e1, e2, x1, x2;
        x1 = this.firstChild.firstChild;
        assert x1.firstChild == null;
        e1 = x1.nextSibling;
        x2 = this.firstChild.nextSibling.firstChild;
        assert x2.firstChild == null;
        e2 = x2.nextSibling;

        Tree lambda = new Lambda();
        lambda.firstChild = x1;
        x1.nextSibling = e2;

        Tree gamma = new Gamma();
        gamma.firstChild = lambda;
        lambda.nextSibling = e1;

        Tree tree = new Eq();
        tree.firstChild = x2;
        x2.nextSibling = gamma;

        tree.nextSibling = this.nextSibling;
        return tree;
    }
}
