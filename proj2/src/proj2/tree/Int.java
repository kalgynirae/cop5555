package proj2.tree;

import proj2.tree.TreeValue;

public class Int extends TreeValue {
    public Int(String value) {
        super(value.substring(5, value.length() - 1));
    }

    public String toString() {
        return "<INT:" + this.value + ">";
    }
}
