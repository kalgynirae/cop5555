package proj2.csem;

public class PrimitiveEnvironment extends Environment {
    PrimitiveEnvironment(boolean debug) {
        super(null, debug);
    }

    public StackItem lookup(String name) {
        if (this.debug) {
            System.err.println("PE found " + name);
        }
        if (name.equals("<nil>")) {
            return new TupleStackItem();
        }
        else if (name.equals("<true>")) {
            return new BoolStackItem(true);
        }
        else if (name.equals("<false>")) {
            return new BoolStackItem(false);
        }
        return new PrimitiveStackItem(name);
    }
}
