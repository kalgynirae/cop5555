package proj2.csem;

class BinopControlItem extends ControlItem {
    String name;

    BinopControlItem(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
