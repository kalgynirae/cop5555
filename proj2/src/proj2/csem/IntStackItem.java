package proj2.csem;

class IntStackItem extends StackItem {
    int value;

    IntStackItem(int value) {
        this.value = value;
    }

    public String toString() {
        return "" + value;
    }
}
