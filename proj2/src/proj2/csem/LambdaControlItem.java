package proj2.csem;

import java.util.ArrayList;

import proj2.tree.Id;
import proj2.tree.Tree;

class LambdaControlItem extends ControlItem {
    ArrayList<String> names;
    ControlStructure cs;

    LambdaControlItem(Tree ids, ControlStructure cs) {
        this.cs = cs;

        // Extract names
        this.names = new ArrayList<String>();
        if (ids instanceof Id) {
            this.names.add(((Id) ids).value);
        }
        else {
            for (Tree t : ids.children()) {
                this.names.add(((Id) t).value);
            }
        }
    }

    public String toString() {
        String names = "";
        for (String name : this.names) {
            names += name + ",";
        }
        names = names.substring(0, names.length() - 1);
        return "L<" + names + ">" + this.cs;
    }
}
