package proj2.csem;

class StrStackItem extends StackItem {
    String value;

    StrStackItem(String s) {
        this.value = s;
    }

    public String toString() {
        return value;
    }
}
