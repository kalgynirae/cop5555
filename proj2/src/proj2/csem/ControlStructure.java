package proj2.csem;

import java.util.ArrayDeque;

class ControlStructure extends ArrayDeque<ControlItem> {
    int n;
    static int total;

    static {
        total = 0;
    }

    ControlStructure() {
        this.n = total++;
    }

    public String toString() {
        String s = "[ ";
        for (ControlItem i : this) {
            s += i + " ";
        }
        return s + "]";
    }
}
