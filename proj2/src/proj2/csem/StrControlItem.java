package proj2.csem;

class StrControlItem extends ControlItem {
    String value;

    StrControlItem(String s) {
        this.value = s;
    }

    public String toString() {
        return value;
    }
}
