package proj2.csem;

import java.util.ArrayList;

class LambdaStackItem extends StackItem {
    ControlStructure cs;
    Environment e;
    ArrayList<String> names;

    LambdaStackItem(ControlItem lci, Environment e) {
        this.e = e;
        this.cs = ((LambdaControlItem) lci).cs;
        this.names = ((LambdaControlItem) lci).names;
    }

    public String toString() {
        //String names = "";
        //for (String name : this.names) {
        //    names += name + ",";
        //}
        //names = names.substring(0, names.length() - 1);
        //return "L<" + e.n + ":" + names + ">" + this.cs;

        String vars = "";
        for (String name : this.names) {
            vars += name + ", ";
        }
        if (vars.length() > 2) {
            vars = vars.substring(0, vars.length() - 2);
        }
        return "[lambda closure: " + vars + ": " + this.cs.n + "]";
    }
}
