package proj2.csem;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import proj2.tree.*;

public class CSEMachine {
    ControlStructure control;
    Stack stack;
    Deque<Environment> environments;
    boolean debug;

    public CSEMachine (Tree t, boolean debug) {
        this.debug = debug;
        this.control = new ControlStructure();
        this.control.addFirst(new EnvControlItem(0));
        this.environments = new ArrayDeque<Environment>();
        this.environments.addFirst(new PrimitiveEnvironment(this.debug));
        generateCS(t, this.control);
    }

    public void run() {
        this.stack = new Stack();
        this.stack.addFirst(new EnvStackItem(0));
        while (this.control.size() > 0) {
            if (this.debug) {
                System.err.println(this.control + " ----- " + this.stack);
            }

            ControlItem ci = this.control.removeLast();

            // Rule 1 - stack name
            if (ci instanceof NameControlItem) {
                String name = ((NameControlItem) ci).name;
                this.stack.addFirst(this.environments.getFirst().lookup(name));
            }

            // Rule 2 - stack lambda-closure
            else if (ci instanceof LambdaControlItem) {
                this.stack.addFirst(new LambdaStackItem(ci,
                                                this.environments.getFirst()));
            }

            else if (ci instanceof GammaControlItem) {

                // Rule 4 & 11 - apply lambda-closure
                if (this.stack.getFirst() instanceof LambdaStackItem) {
                    LambdaStackItem lsi = (LambdaStackItem) this.stack.removeFirst();
                    Environment e = new Environment(lsi.e, this.debug);
                    this.environments.addFirst(e);
                    StackItem values = this.stack.removeFirst();
                    if (lsi.names.size() == 1) {
                        e.set(lsi.names.get(0), values);
                    }
                    else {
                        int i = 0;
                        for (String name : lsi.names) {
                            e.set(name, ((TupleStackItem) values).items.get(i));
                            i++;
                        }
                    }

                    this.control.addLast(new EnvControlItem(e.n));
                    for (ControlItem c : lsi.cs) {
                        this.control.addLast(c);
                    }
                    this.stack.addFirst(new EnvStackItem(e.n));
                }

                // Rule 10 - select tuple
                else if (this.stack.getFirst() instanceof TupleStackItem) {
                    TupleStackItem tuple = ((TupleStackItem) this.stack.removeFirst());
                    int n = ((IntStackItem) this.stack.removeFirst()).value;
                    this.stack.addFirst(tuple.items.get(n - 1));
                }

                // Rule something - something
                else if (this.stack.getFirst() instanceof EtaStackItem) {
                    EtaStackItem eti = (EtaStackItem) this.stack.getFirst();
                    this.stack.addFirst(eti.lsi);
                    this.control.addLast(new GammaControlItem());
                    this.control.addLast(new GammaControlItem());
                }

                // Rule 3 - apply rator
                else {
                    StackItem rator = this.stack.removeFirst();
                    StackItem rand = this.stack.removeFirst();
                    if (this.debug) {
                        System.out.println("Apply " + rator + " to " + rand);
                    }
                    String name = ((PrimitiveStackItem) rator).name;
                    this.stack.addFirst(doRator(name, rand));
                }
            }

            // Rule 5 - exit environment
            else if (ci instanceof EnvControlItem) {
                StackItem value = this.stack.removeFirst();
                StackItem env = this.stack.removeFirst();
                assert env instanceof EnvStackItem;
                this.environments.removeFirst();
                this.stack.addFirst(value);
            }

            // Rule 6 - binary operator
            else if (ci instanceof BinopControlItem) {
                StackItem rand1 = this.stack.removeFirst();
                StackItem rand2 = this.stack.removeFirst();
                if (this.debug) {
                    System.out.println("Doing " + ci + " to " + rand1 +
                                       " and " + rand2);
                }
                StackItem result = doBinop(ci, rand1, rand2);
                this.stack.addFirst(result);
            }

            // Rule 7 - unary operator
            else if (ci instanceof UnopControlItem) {
                StackItem rand = this.stack.removeFirst();
                if (this.debug) {
                    System.out.println("Doing " + ci + " to " + rand);
                }
                StackItem result = doUnop(ci, rand);
                this.stack.addFirst(result);
            }

            // Rule 8 - conditional
            else if (ci instanceof BetaControlItem) {
                boolean r = ((BoolStackItem) this.stack.removeFirst()).value;
                ControlItem selected;
                if (r) {
                    this.control.removeLast();
                    selected = this.control.removeLast();
                }
                else {
                    selected = this.control.removeLast();
                    this.control.removeLast();
                }
                for (ControlItem c : ((DeltaControlItem) selected).cs) {
                    this.control.addLast(c);
                }
            }

            // Rule 9 - form tuple
            else if (ci instanceof TauControlItem) {
                TupleStackItem tsi = new TupleStackItem();
                int n = ((TauControlItem) ci).n;
                for (int i = 0; i < n; i++) {
                    tsi.addItem(this.stack.removeFirst());
                }
                this.stack.addFirst(tsi);
            }

            // Plain values
            else if (ci instanceof IntControlItem) {
                int value = ((IntControlItem) ci).value;
                this.stack.addFirst(new IntStackItem(value));
            }
            else if (ci instanceof StrControlItem) {
                String value = ((StrControlItem) ci).value;
                this.stack.addFirst(new StrStackItem(value));
            }

            else {
                assert false; // Unknown ControlItem
            }
        }
        assert this.stack.size() == 1;
    }

    static void generateCS(Tree t, ControlStructure cs) {
        if (t instanceof Gamma) {
            cs.addLast(new GammaControlItem());
        }
        else if (t instanceof Lambda) {
            // Get the control structure for the child
            ControlStructure lcs = new ControlStructure();
            generateCS(t.firstChild.nextSibling, lcs);
            LambdaControlItem lci = new LambdaControlItem(t.firstChild, lcs);
            cs.addLast(lci);
            // Skip processing children
            return;
        }
        else if (t instanceof Cond) {
            ControlStructure csthen = new ControlStructure();
            generateCS(t.firstChild.nextSibling, csthen);
            DeltaControlItem dthen = new DeltaControlItem(csthen);
            cs.addLast(dthen);

            ControlStructure cselse = new ControlStructure();
            generateCS(t.firstChild.nextSibling.nextSibling, cselse);
            DeltaControlItem delse = new DeltaControlItem(cselse);
            cs.addLast(delse);

            cs.addLast(new BetaControlItem());
            generateCS(t.firstChild, cs);
            // Skip children
            return;
        }
        else if (t instanceof Tau) {
            cs.addLast(new TauControlItem(t.children().size()));
        }
        else if (t instanceof Binop) {
            cs.addLast(new BinopControlItem(t.toString()));
        }
        else if (t instanceof Unop) {
            cs.addLast(new UnopControlItem(t.toString()));
        }
        else if (t instanceof Int) {
            cs.addLast(new IntControlItem(((Int) t).value));
        }
        else if (t instanceof Str) {
            String value = ((Str) t).value;
            cs.addLast(new StrControlItem(value));
        }
        else if (t instanceof Id) {
            cs.addLast(new NameControlItem(((Id) t).value));
        }
        else if (t instanceof Value) {
            cs.addLast(new NameControlItem(((Value) t).value));
        }
        else if (t instanceof Aug) {
            cs.addLast(new BinopControlItem("aug"));
        }
        else {
            System.out.println(t);
            assert false; // Unknown Tree node in control structure generation
        }

        // Process children
        for (Tree child : t.children()) {
            generateCS(child, cs);
        }
    }

    static StackItem doBinop(ControlItem binop, StackItem rand1, StackItem rand2) {
        String op = ((BinopControlItem) binop).name;
        if (("eq ne").contains(op)) {
            // These can work for any type
            if (rand1 instanceof IntStackItem) {
                int r1 = ((IntStackItem) rand1).value;
                int r2 = ((IntStackItem) rand2).value;
                if (op.equals("eq")) return new BoolStackItem(r1 == r2);
                else if (op.equals("ne")) return new BoolStackItem(r1 != r2);
            }
            else if (rand1 instanceof StrStackItem) {
                String r1 = ((StrStackItem) rand1).value;
                String r2 = ((StrStackItem) rand2).value;
                if (op.equals("eq")) return new BoolStackItem(r1.equals(r2));
                else if (op.equals("ne")) return new BoolStackItem(!r1.equals(r2));
            }
            else if (rand1 instanceof BoolStackItem) {
                boolean r1 = ((BoolStackItem) rand1).value;
                boolean r2 = ((BoolStackItem) rand2).value;
                if (op.equals("eq")) return new BoolStackItem(r1 == r2);
                else if (op.equals("ne")) return new BoolStackItem(r1 != r2);
            }
        }
        else if (("+ - / * **").contains(op)) {
            int r1 = ((IntStackItem) rand1).value;
            int r2 = ((IntStackItem) rand2).value;
            int result = -1;
            if (op.equals("+")) result = r1 + r2;
            else if (op.equals("-")) result = r1 - r2;
            else if (op.equals("*")) result = r1 * r2;
            else if (op.equals("/")) result = r1 / r2;
            else if (op.equals("**")) result = (int) Math.pow(r1, r2);
            else assert false; // Uh oh
            return new IntStackItem(result);
        }
        else if (("gr ge ls le").contains(op)) {
            int r1 = ((IntStackItem) rand1).value;
            int r2 = ((IntStackItem) rand2).value;
            boolean result = false;
            if (op.equals("gr")) result = r1 > r2;
            else if (op.equals("ge")) result = r1 >= r2;
            else if (op.equals("ls")) result = r1 < r2;
            else if (op.equals("le")) result = r1 <= r2;
            else if (op.equals("eq")) result = r1 == r2;
            else if (op.equals("ne")) result = r1 != r2;
            else assert false; // Uh oh
            return new BoolStackItem(result);
        }
        else if (("& or").contains(op)) {
            boolean r1 = ((BoolStackItem) rand1).value;
            boolean r2 = ((BoolStackItem) rand2).value;
            boolean result = false;
            if (op.equals("&")) result = r1 && r2;
            else if (op.equals("or")) result = r1 || r2;
            else assert false; // Uh oh
            return new BoolStackItem(result);
        }
        else if (op.equals("aug")) {
            TupleStackItem tuple = (TupleStackItem) rand1;
            TupleStackItem newtuple = new TupleStackItem(tuple);
            newtuple.addItem(rand2);
            return newtuple;
        }
        System.out.println("Unknown binop: " + op);
        assert false; // Unknown operator
        return new IntStackItem(-42);
    }

    static StackItem doUnop(ControlItem unop, StackItem rand) {
        String op = ((UnopControlItem) unop).name;
        if (op.equals("not")) {
            boolean r = ((BoolStackItem) rand).value;
            return new BoolStackItem(!r);
        }
        else if (op.equals("neg")) {
            int r = ((IntStackItem) rand).value;
            return new IntStackItem(-r);
        }
        System.out.println("Unknown unop: " + op);
        assert false;
        return new IntStackItem(-42);
    }

    StackItem doRator(String name, StackItem rand) {
        if (name.equals("Print")) {
            Printer.print(rand);
            return new PrimitiveStackItem("dummy");
        }
        else if (name.equals("Isstring")) {
            return new BoolStackItem(rand instanceof StrStackItem);
        }
        else if (name.equals("Isinteger")) {
            return new BoolStackItem(rand instanceof IntStackItem);
        }
        else if (name.equals("Istruthvalue")) {
            return new BoolStackItem(rand instanceof BoolStackItem);
        }
        else if (name.equals("Istuple")) {
            return new BoolStackItem(rand instanceof TupleStackItem);
        }
        else if (name.equals("Isfunction")) {
            return new BoolStackItem(rand instanceof LambdaStackItem);
        }
        else if (name.equals("Isdummy")) {
            return new BoolStackItem(rand instanceof PrimitiveStackItem &&
                    ((PrimitiveStackItem) rand).name.equals("dummy"));
        }
        else if (name.equals("<Y*>")) {
            LambdaStackItem lsi = (LambdaStackItem) rand;
            return new EtaStackItem(lsi);
        }
        else if (name.equals("Conc")) {
            String s1 = ((StrStackItem) rand).value;
            String s2 = ((StrStackItem) this.stack.removeFirst()).value;
            this.control.removeLast(); // Note: sketchy
            return new StrStackItem(s1 + s2);
        }
        else if (name.equals("Stem")) {
            String s = ((StrStackItem) rand).value;
            try {
                return new StrStackItem(s.substring(0, 1));
            }
            catch (java.lang.StringIndexOutOfBoundsException e) {
                return new StrStackItem("");
            }
        }
        else if (name.equals("Stern")) {
            String s = ((StrStackItem) rand).value;
            try {
                return new StrStackItem(s.substring(1));
            }
            catch (java.lang.StringIndexOutOfBoundsException e) {
                return new StrStackItem("");
            }
        }
        else if (name.equals("Order")) {
            TupleStackItem tsi = (TupleStackItem) rand;
            return new IntStackItem(tsi.items.size());
        }
        else if (name.equals("Null")) {
            TupleStackItem tsi = (TupleStackItem) rand;
            return new BoolStackItem(tsi.items.size() == 0);
        }
        else if (name.equals("ItoS")) {
            IntStackItem i = (IntStackItem) rand;
            return new StrStackItem("" + i.value);
        }
        System.out.println("Unknown rator: " + name);
        assert false;
        return rand;
    }
}
