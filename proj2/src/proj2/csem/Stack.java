package proj2.csem;

import java.util.ArrayDeque;

class Stack extends ArrayDeque<StackItem> {
    public String toString() {
        String s = "[ ";
        for (StackItem i : this) {
            s += i + " ";
        }
        return s + "]";
    }
}
