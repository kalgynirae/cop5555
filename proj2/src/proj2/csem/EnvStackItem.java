package proj2.csem;

class EnvStackItem extends StackItem {
    int n;

    EnvStackItem(int n) {
        this.n = n;
    }

    public String toString() {
        return "e" + n;
    }
}
