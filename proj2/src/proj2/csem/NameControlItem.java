package proj2.csem;

class NameControlItem extends ControlItem {
    String name;

    NameControlItem(String name) {
        this.name = name;
    }

    public String toString() {
        return "'" + this.name + "'";
    }
}
