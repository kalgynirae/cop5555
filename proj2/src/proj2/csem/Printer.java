package proj2.csem;

public class Printer {
    public static boolean noout;

    static {
        noout = false;
    }

    static void print(StackItem s) {
        if (!noout) {
            if (s instanceof TupleStackItem) {
                String values = "";
                for (StackItem si : ((TupleStackItem) s).items) {
                    values += si + ", ";
                }
                values = values.substring(0, values.length() - 2);
                System.out.print("(" + values + ")");
            }
            else {
                System.out.print(s);
            }
        }
    }
}
