package proj2.csem;

class UnopControlItem extends ControlItem {
    String name;

    UnopControlItem(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
