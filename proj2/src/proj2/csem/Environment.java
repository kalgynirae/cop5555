package proj2.csem;

import java.util.HashMap;

public class Environment {
    boolean debug;
    static int total;
    int n;
    Environment parent;
    HashMap<String, StackItem> bindings;

    static {
        total = 0;
    }

    public Environment(Environment parent, boolean debug) {
        this.debug = debug;
        this.bindings = new HashMap<String, StackItem>();
        this.n = total++;
        this.parent = parent;
    }

    public void set(String name, StackItem value) {
        this.bindings.put(name, value);
    }

    public StackItem lookup(String name) {
        if (this.debug) {
            System.err.print("Looking up " + name + " in e" + this.n + "...");
        }

        if (this.bindings.containsKey(name)) {
            if (this.debug) {
                System.err.println("Found " + this.bindings.get(name));
            }
            return this.bindings.get(name);
        }

        // Every environment other than the PE will have a parent
        return this.parent.lookup(name);
    }
}
