package proj2.csem;

class TauControlItem extends ControlItem {
    int n;

    TauControlItem(int n) {
        this.n = n;
    }

    public String toString() {
        return "tau" + this.n;
    }
}
