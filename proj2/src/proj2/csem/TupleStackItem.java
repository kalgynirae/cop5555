package proj2.csem;

import java.util.ArrayList;

class TupleStackItem extends StackItem {
    ArrayList<StackItem> items;

    TupleStackItem() {
        this.items = new ArrayList<StackItem>();
    }

    TupleStackItem(TupleStackItem other) {
        this.items = new ArrayList<StackItem>();
        for (StackItem i : other.items) {
            this.items.add(i);
        }
    }

    void addItem(StackItem i) {
        this.items.add(i);
    }

    public String toString() {
        return "t" + this.items.size();
    }
}
