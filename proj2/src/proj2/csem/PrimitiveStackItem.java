package proj2.csem;

class PrimitiveStackItem extends StackItem {
    String name;

    PrimitiveStackItem(String name) {
        this.name = name;
    }

    public String toString() {
        return "(" + this.name + ")";
    }
}
