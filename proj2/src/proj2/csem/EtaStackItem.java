package proj2.csem;

class EtaStackItem extends StackItem {
    LambdaStackItem lsi;

    EtaStackItem(LambdaStackItem lsi) {
        this.lsi = lsi;
    }

    public String toString() {
        return this.lsi.toString().replace("L", "E");
    }
}
