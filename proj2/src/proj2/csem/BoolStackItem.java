package proj2.csem;

class BoolStackItem extends StackItem {
    boolean value;

    BoolStackItem(boolean value) {
        this.value = value;
    }

    public String toString() {
        return "" + this.value;
    }
}
