package proj2.csem;

import proj2.tree.Tree;

class DeltaControlItem extends ControlItem {
    ControlStructure cs;

    DeltaControlItem(ControlStructure cs) {
        this.cs = cs;
    }

    public String toString() {
        return "delta" + this.cs;
    }
}
