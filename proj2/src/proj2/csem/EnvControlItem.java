package proj2.csem;

class EnvControlItem extends ControlItem {
    int n;

    EnvControlItem(int n) {
        this.n = n;
    }

    public String toString() {
        return "e" + n;
    }
}
