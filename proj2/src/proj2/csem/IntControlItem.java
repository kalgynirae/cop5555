package proj2.csem;

class IntControlItem extends ControlItem {
    int value;

    IntControlItem(String s) {
        this.value = Integer.parseInt(s);
    }

    public String toString() {
        return "" + value;
    }
}
