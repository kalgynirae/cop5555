package proj2;

import java.io.IOException;
import java.util.EmptyStackException;
import java.util.Stack;

import proj2.ParseError;
import proj2.tree.*;

public class Parser {
    Tree ast;
    boolean debug;
    Tokenizer tokenizer;
    Stack<Tree> trees;

    public Parser(Tokenizer tokenizer, boolean debug) throws ParseError {
        this.debug = debug;
        this.tokenizer = tokenizer;
        this.trees = new Stack<Tree>();
        E();
        consume("eof");
        try {
            this.ast = this.trees.pop();
        }
        catch (EmptyStackException e) {
            throw new ParseError("Ended with no trees on the stack");
        }
        if (!this.trees.empty()) {
            throw new ParseError("Ended with multiple trees on the stack");
        }
    }

    public Tree getAST() {
        return this.ast;
    }

    public void printAST() {
        this.ast.print("");
    }

    void buildTree(Tree root, int subtreeCount) {
        if (this.debug) {
            System.out.println("buildTree(" + root + ", " + subtreeCount + ")");
        }
        if (subtreeCount > 0) {
            Tree t = this.trees.pop();
            for (int i = 1; i < subtreeCount; i++) {
                Tree q = this.trees.pop();
                q.nextSibling = t;
                t = q;
            }
            root.firstChild = t;
        }
        this.trees.push(root);
    }

    void consume(String type) throws ParseError {
        Token t = new Token(type);
        if (this.tokenizer.peek().equals(t)) {
            this.tokenizer.readNextToken();
        }
        else {
            throw new ParseError("[consume] Expected " + t + "; got " + this.tokenizer.peek());
        }
    }

    boolean tryConsume(String type) throws ParseError {
        Token t = new Token(type);
        if (this.tokenizer.peek().equals(t)) {
            this.tokenizer.readNextToken();
            return true;
        }
        else {
            return false;
        }
    }

    void readId() throws ParseError {
        Token read = this.tokenizer.peek();
        if (read.equals(new Token("ID"))) {
            buildTree(new Id(read.toString()), 0);
            this.tokenizer.readNextToken();
        }
        else {
            throw new ParseError("[read] Expected <ID>; got " + read);
        }
    }

    boolean tryRead(String type) throws ParseError {
        Token t = new Token(type);
        Token read = this.tokenizer.peek();
        if (read.equals(t)) {
            Tree q;
            if (type.equals("ID")) {
                q = new Id(read.toString());
            }
            else if (type.equals("INT")) {
                q = new Int(read.toString());
            }
            else {
                q = new Str(read.value);
            }
            buildTree(q, 0);
            this.tokenizer.readNextToken();
            return true;
        }
        else {
            return false;
        }
    }

    boolean test(String type) throws ParseError {
        Token t = new Token(type);
        return this.tokenizer.peek().equals(t);
    }

    void E() throws ParseError {
        if (tryConsume("let")) {
            D();
            consume("in");
            E();
            buildTree(new Let(), 2);
        }
        else if (tryConsume("fn")) {
            Vb();
            int n = 1;
            while (!tryConsume(".")) {
                Vb();
                n++;
            }
            E();
            buildTree(new Lambda(), n + 1);
        }
        else {
            Ew();
        }
    }

    void Ew() throws ParseError {
        T();
        if (tryConsume("where")) {
            Dr();
            buildTree(new Where(), 2);
        }
    }

    void T() throws ParseError {
        Ta();
        int n = 1;
        while (tryConsume(",")) {
            Ta();
            n++;
        }
        if (n > 1) {
            buildTree(new Tau(), n);
        }
    }

    void Ta() throws ParseError {
        Tc();
        while (tryConsume("aug")) {
            Tc();
            buildTree(new Aug(), 2);
        }
    }

    void Tc() throws ParseError {
        B();
        if (tryConsume("->")) {
            Tc();
            consume("|");
            Tc();
            buildTree(new Cond(), 3);
        }
    }

    void B() throws ParseError {
        Bt();
        while (tryConsume("or")) {
            Bt();
            buildTree(new Binop("or"), 2);
        }
    }

    void Bt() throws ParseError {
        Bs();
        while (tryConsume("&")) {
            Bs();
            buildTree(new Binop("&"), 2);
        }
    }

    void Bs() throws ParseError {
        if (tryConsume("not")) {
            Bp();
            buildTree(new Unop("not"), 1);
        }
        else {
            Bp();
        }
    }

    void Bp() throws ParseError {
        A();
        if (tryConsume("gr") || tryConsume(">")) {
            A();
            buildTree(new Binop("gr"), 2);
        }
        else if (tryConsume("ge") || tryConsume(">=")) {
            A();
            buildTree(new Binop("ge"), 2);
        }
        else if (tryConsume("ls") || tryConsume("<")) {
            A();
            buildTree(new Binop("ls"), 2);
        }
        else if (tryConsume("le") || tryConsume("<=")) {
            A();
            buildTree(new Binop("le"), 2);
        }
        else if (tryConsume("eq")) {
            A();
            buildTree(new Binop("eq"), 2);
        }
        else if (tryConsume("ne")) {
            A();
            buildTree(new Binop("ne"), 2);
        }
    }

    void A() throws ParseError {
        if (tryConsume("+")) {
            At();
        }
        else if (tryConsume("-")) {
            At();
            buildTree(new Unop("neg"), 1);
        }
        else {
            At();
        }
        while (true) {
            if (tryConsume("+")) {
                At();
                buildTree(new Binop("+"), 2);
            }
            else if (tryConsume("-")) {
                At();
                buildTree(new Binop("-"), 2);
            }
            else {
                break;
            }
        }
    }

    void At() throws ParseError {
        Af();
        while (true) {
            if (tryConsume("*")) {
                Af();
                buildTree(new Binop("*"), 2);
            }
            else if (tryConsume("/")) {
                Af();
                buildTree(new Binop("/"), 2);
            }
            else {
                break;
            }
        }
    }

    void Af() throws ParseError {
        Ap();
        if (tryConsume("**")) {
            Af();
            buildTree(new Binop("**"), 2);
        }
    }

    void Ap() throws ParseError {
        R();
        while (tryConsume("@")) {
            readId();
            R();
            buildTree(new At(), 3);
        }
    }

    void R() throws ParseError {
        Rn();
        while (true) {
            try {
                Rn();
            }
            catch (ParseError e) {
                break;
            }
            buildTree(new Gamma(), 2);
        }
    }

    void Rn() throws ParseError {
        if (tryRead("ID")) {
        }
        else if (tryRead("INT")) {
        }
        else if (tryRead("STR")) {
        }
        else if (tryConsume("true")) {
            buildTree(new Value("<true>"), 0);
        }
        else if (tryConsume("false")) {
            buildTree(new Value("<false>"), 0);
        }
        else if (tryConsume("nil")) {
            buildTree(new Value("<nil>"), 0);
        }
        else if (tryConsume("(")) {
            E();
            consume(")");
        }
        else if (tryConsume("dummy")) {
            buildTree(new Value("<dummy>"), 0);
        }
        else {
            throw new ParseError("[Rn] Expected ID, INT, STR, true, false, nil, (, or dummy");
        }
    }

    void D() throws ParseError {
        Da();
        if (tryConsume("within")) {
            D();
            buildTree(new Within(), 2);
        }
    }

    void Da() throws ParseError {
        Dr();
        int n = 1;
        while (tryConsume("and")) {
            Dr();
            n++;
        }
        if (n > 1) {
            buildTree(new And(), n);
        }
    }

    void Dr() throws ParseError {
        if (tryConsume("rec")) {
            Db();
            buildTree(new Rec(), 1);
        }
        else {
            Db();
        }
    }

    void Db() throws ParseError {
        if (test("ID")) {
            boolean multiple = Vl();
            if (multiple || test("=")) {
                consume("=");
                E();
                buildTree(new Eq(), 2);
            }
            else {
                Vb();
                int n = 1;
                while (!tryConsume("=")) {
                    Vb();
                    n++;
                }
                E();
                buildTree(new FcnForm(), n + 2);
            }
        }
        else if (tryConsume("(")) {
            D();
            consume(")");
        }
        else {
            throw new ParseError("[Db] Expected ID or (");
        }
    }

    void Vb() throws ParseError {
        if (tryRead("ID")) {
        }
        else if (tryConsume("(")) {
            if (tryConsume(")")) {
                buildTree(new Value("()"), 0);
            }
            else {
                Vl();
                consume(")");
            }
        }
        else {
            throw new ParseError("[Vb] Expected ID or (");
        }
    }

    boolean Vl() throws ParseError {
        readId();
        int n = 1;
        while (tryConsume(",")) {
            readId();
            n++;
        }
        if (n > 1) {
            buildTree(new Binop(","), n);
            return true;
        }
        else {
            return false;
        }
    }
}
