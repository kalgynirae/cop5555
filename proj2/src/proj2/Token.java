package proj2;

class Token {
    String type;
    String value = null;

    Token(String type) {
        this.type = type;
    }

    Token(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public boolean equals(Token other) {
        if (other.value == null) {
            return this.type.equals(other.type);
        }
        else {
            return (this.type.equals(other.type) &&
                    this.value != null && this.value.equals(other.value));
        }
    }

    public boolean isA(String type) {
        return this.type.equals(type);
    }

    public String toString() {
        if (this.value != null) {
            if (this.type.equals("STR")) {
                String fixed = this.value.replace("\\", "\\\\")
                                         .replace("'", "\\'")
                                         .replace("\n", "\\n")
                                         .replace("\t", "\\t");
                return "<STR:'" + fixed + "'>";
            }
            return "<" + this.type + ":" + this.value + ">";
        }
        else {
            return this.type;
        }
    }
}
