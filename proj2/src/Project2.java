import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import proj2.csem.CSEMachine;
import proj2.csem.Printer;
import proj2.ParseError;
import proj2.Parser;
import proj2.Tokenizer;
import proj2.tree.Tree;

class Project2 {
    static void printUsage() {
        System.err.println("Usage: p2 [-ast] [-debug] [-st] " +
                           "[-noout] FILE");
    }

    public static void main(String[] args) {
        HashMap<String, String> options = parseArgs(args);

        try {
            FileReader in = new FileReader(options.get("filename"));
            //Tokenizer tk = new Tokenizer(in, options.get("-debug").equals("true"));
            Tokenizer tk = new Tokenizer(in, false);
            Parser parser = new Parser(tk, false);

            Tree t = parser.getAST();
            if (options.get("-ast").equals("true")) {
                t.print("");
            }

            t = t.standardize(false);

            if (options.get("-st").equals("true")) {
                t.print("");
            }

            // Set Noout
            Printer.noout = options.get("-noout").equals("true");
            CSEMachine c = new CSEMachine(t, options.get("-debug").equals("true"));
            c.run();

            if (!Printer.noout) {
                System.out.println(); // Blank line, like reference implementation
            }
        }
        catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
            System.exit(2);
        }
        catch (ParseError e) {
            System.err.println("ParseError: " + e.getMessage());
            System.exit(3);
        }
    }

    static HashMap<String, String> parseArgs(String[] args) {
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("-ast", "false");
        options.put("-st", "false");
        options.put("-debug", "false");
        options.put("-noout", "false");

        for (String arg : args) {
            if (arg.startsWith("-")) {
                if (options.containsKey(arg)) {
                    options.put(arg, "true");
                }
                else {
                    // Invalid switch. Ignore it, like the reference parser
                    // does.
                }
            }
            // The reference parser always treats the last argument as the
            // filename, even if it starts with a hyphen. This simulates that
            // behavior.
            options.put("filename", arg);
        }

        if (options.get("filename") == null) {
            printUsage();
            System.exit(1);
        }
        return options;
    }
}
