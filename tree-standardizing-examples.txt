    let
    /  \
   =    gamma..
  / \
 x   3

standardizes to

     gamma
    /     \
 lambda    3
  / \
 x   gamma..

---

      where
     /     \
  where     =
  /   \     / \
 +     =   y  4
/ \   / \
x  y  x  3

std to

    where
    /   \
gamma
/      [incomplete]
lambda
/     \
x     gamma
      /    \
      x   y

---

                   let
        within           y
     =      fcn_form   f   2
   c  3    f  x   +
                 x c

standardizes to

                   let
        =                        y
    f       y                   f 2
         h     3
        c  h
          x  h
            x c

standardizes to

                   y
        h                   y
    f       y            h     3
          f   2         c  h
                          x  h
                            x c

