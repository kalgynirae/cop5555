import re
import sys

def E0():
    write('E0 -> E1 X0')
    E1()
    X0()

def X0():
    if next_token == '|':
        write('X0 -> | E1 X0')
        read('|')
        E1()
        X0()
    else:
        write('X0 ->')

def E1():
    write('E1 -> E2 X1')
    E2()
    X1()

def X1():
    if next_token in ['(', 'a', 'b', 'c']:
        write('X1 -> E2 X1')
        E2()
        X1()
    else:
        write('X1 ->')

def E2():
    write('E2 -> E3 X2')
    E3()
    X2()

def X2():
    if next_token == 'list':
        write('X2 -> list E3 X2')
        read('list')
        E3()
        X2()
    else:
        write('X2 ->')

def E3():
    write('E3 -> E4 X3')
    E4()
    X3()

def X3():
    if next_token == '+':
        write('X3 -> +')
        read('+')
    elif next_token == '*':
        write('X3 -> *')
        read('*')
    elif next_token == '?':
        write('X3 -> ?')
        read('?')
    else:
        write('X3 ->')

def E4():
    if next_token == '(':
        write('E4 -> ( E0 )')
        read('(')
        E0()
        read(')')
    elif next_token == 'a':
        write('E4 -> a')
        read('a')
    elif next_token == 'b':
        write('E4 -> b')
        read('b')
    elif next_token == 'c':
        write('E4 -> c')
        read('c')

def read(t):
    global next_token
    if next_token == t:
        next_token = next(tokens, None)
    else:
        raise SyntaxError("Expected %r; found %r" % (t, next_token))

def write(rule):
    print(rule)

for line in sys.stdin:
    tokens = (t for t in re.split(r"(\W)", line) if t.strip())
    next_token = next(tokens, None)
    try:
        E0()
        read(None)
    except SyntaxError as e:
        print("Syntax error: %s" % e)
