import re
import sys

def E0():
    E1()
    while next_token == '|':
        read('|')
        E1()
        build_tree('|', 2)

def E1():
    E2()
    if next_token in ['(', 'a', 'b', 'c']:
        E1()
        build_tree('cat', 2)

def E2():
    E3()
    while next_token == 'list':
        read('list')
        E3()
        build_tree('list', 2)

def E3():
    E4()
    if next_token == '+':
        read('+')
        build_tree('+', 1)
    elif next_token == '*':
        read('*')
        build_tree('*', 1)
    elif next_token == '?':
        read('?')
        build_tree('?', 1)

def E4():
    if next_token == '(':
        read('(')
        E0()
        read(')')
    elif next_token == 'a':
        read('a')
        build_tree('a', 0)
    elif next_token == 'b':
        read('b')
        build_tree('b', 0)
    elif next_token == 'c':
        read('c')
        build_tree('c', 0)

def read(t):
    global next_token
    if next_token == t:
        next_token = next(tokens, None)
    else:
        raise SyntaxError("Expected %r; found %r" % (t, next_token))

def build_tree(name, children):
    print(name, children)

for line in sys.stdin:
    tokens = (t for t in re.split(r"(\W)", line) if t.strip())
    next_token = next(tokens, None)
    try:
        E0()
        read(None)
    except SyntaxError as e:
        print("Syntax error: %s" % e)
