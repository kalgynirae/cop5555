import re
import sys

def E0():
    E1()
    X0()
    write('E0 -> E1 X0')

def X0():
    if next_token == '|':
        read('|')
        E1()
        X0()
        write('X0 -> | E1 X0')
    else:
        write('X0 ->')

def E1():
    E2()
    X1()
    write('E1 -> E2 X1')

def X1():
    if next_token in ['(', 'a', 'b', 'c']:
        E2()
        X1()
        write('X1 -> E2 X1')
    else:
        write('X1 ->')

def E2():
    E3()
    X2()
    write('E2 -> E3 X2')

def X2():
    if next_token == 'list':
        read('list')
        E3()
        X2()
        write('X2 -> list E3 X2')
    else:
        write('X2 ->')

def E3():
    E4()
    X3()
    write('E3 -> E4 X3')

def X3():
    if next_token == '+':
        read('+')
        write('X3 -> +')
    elif next_token == '*':
        read('*')
        write('X3 -> *')
    elif next_token == '?':
        read('?')
        write('X3 -> ?')
    else:
        write('X3 ->')

def E4():
    if next_token == '(':
        read('(')
        E0()
        read(')')
        write('E4 -> ( E0 )')
    elif next_token == 'a':
        read('a')
        write('E4 -> a')
    elif next_token == 'b':
        read('b')
        write('E4 -> b')
    elif next_token == 'c':
        read('c')
        write('E4 -> c')

def read(t):
    global next_token
    if next_token == t:
        next_token = next(tokens, None)
    else:
        raise SyntaxError("Expected %r; found %r" % (t, next_token))

def write(rule):
    print(rule)

for line in sys.stdin:
    tokens = (t for t in re.split(r"(\W)", line) if t.strip())
    next_token = next(tokens, None)
    try:
        E0()
        read(None)
    except SyntaxError as e:
        print("Syntax error: %s" % e)
